import Vuex from 'vuex';
import Vue from 'vue';
// import todos from './modules/todos';
import  modules  from './modules'

// Load Vuex
Vue.use(Vuex);

// Create store
export default new Vuex.Store({
  modules: {
    namespaced: true,
    ...modules
  }
});
